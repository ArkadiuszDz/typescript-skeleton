const browserSync = require('browser-sync').create();
const gulp = require('gulp');

gulp.task('browserSync', function() {
    browserSync.init({
      server: {
        baseDir: 'web'
      },
      files: ['app/**/*']
    })
  });