const gulp = require('gulp');
const ts = require('gulp-typescript');
const tsProject = ts.createProject("tsconfig.json");
const browserSync = require('browser-sync');

gulp.task('scripts', () => {

    let tsResult = tsProject.src()
        .pipe(tsProject());
 
    return tsResult.js.pipe(gulp.dest('./web/utils/js')).pipe(browserSync.reload({
        stream: true
    }));;
});