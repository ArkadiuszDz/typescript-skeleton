const gulp = require('gulp');
const browserSync = require('browser-sync');

gulp.task('page',function(){
    return gulp.src('app/views/**/*.html')
    .pipe(gulp.dest('web/'))
    .pipe(browserSync.reload({
        stream: true
    }))
});

gulp.task('fonts',function(){
    return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('web/utils/fonts/'))
    .pipe(browserSync.reload({
        stream: true
    }));
});

gulp.task('images',function(){
    return gulp.src('app/assets/images/**/*')
    .pipe(gulp.dest('web/utils/img/'))
    .pipe(browserSync.reload({
        stream: true
    }));
});