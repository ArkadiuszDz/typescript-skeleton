
const variable : number = 12;
let my : string = "jakiś napis";
console.log(variable);

console.log(my);

let my_object : { name : string; age : number; employed : boolean};

console.log(my_object);

my_object = {
    name: "Bob",
    age: 50,
    employed: false
}

interface Person {
    name : string,
    age : number
}

interface Car {
    brand : string,
    model : string,
    horsepower : number,
    color : string
}

type union_type = Person | Car;

let new_var : union_type = {
    name : "Red",
    age : 54
}

console.log(new_var);

type intersection_type = Person & Car;

let n : intersection_type = {
    brand: "BMW",
    model: "M5",
    horsepower: 360,
    color: "silver",
    name: "Warrior",
    age: 3
};

console.log(n);

const f = () => {
    console.log(this, 'f');
}; 

f();

const a = {
    h() {
        console.log(this, 'h');
    }
}

a.h();

console.log(this);

let array : number[] = [1,4,3,5,7,8,7];

console.log(array);

let array1 : Array<string> = ["asd", "dsds", "sdfd"];

console.log(array1);

let v1 : string = "d";

console.log(v1);

v1 = "2";

console.log(v1);

const fun = (a? : string) : void => {
    console.log('void function', a);
}

fun();

function funkcja (a: number) : number {
    return 2 * a;
}

const funn = (a: number) => (b: number) => {
    return a*b;
};

console.log(funn(8)(9));

function dodaj (a: number) : ((b: number) => number) {   // ((b: number) => number) - typ zwracanej wartości - w tym przypadku funkcja
    return (b: number) => 4*a*5*b;
}

console.log(dodaj(9)(8));

const vfun = (a: number) : number => 7*a;

const fun1 = (a: number) => {
    return function (b: number) {
        return 2*a + b;
    }
}

console.log(fun1(9)(2));

class Song {
    private title: string = "Highway to Hell";
    private author: string = "ACDC";
    y() {
      return this;
    }
}

const song = new Song();
console.log(song.y());