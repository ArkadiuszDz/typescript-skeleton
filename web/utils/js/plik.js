var _this = this;
var variable = 12;
var my = "jakiś napis";
console.log(variable);
console.log(my);
var my_object;
console.log(my_object);
my_object = {
    name: "Bob",
    age: 50,
    employed: false
};
var new_var = {
    name: "Red",
    age: 54
};
console.log(new_var);
var n = {
    brand: "BMW",
    model: "M5",
    horsepower: 360,
    color: "silver",
    name: "Warrior",
    age: 3
};
console.log(n);
var f = function () {
    console.log(_this, 'f');
};
f();
var a = {
    h: function () {
        console.log(this, 'h');
    }
};
a.h();
console.log(this);
var array = [1, 4, 3, 5, 7, 8, 7];
console.log(array);
var array1 = ["asd", "dsds", "sdfd"];
console.log(array1);
var v1 = "d";
console.log(v1);
v1 = "2";
console.log(v1);
var fun = function (a) {
    console.log('void function', a);
};
fun();
function funkcja(a) {
    return 2 * a;
}
var funn = function (a) { return function (b) {
    return a * b;
}; };
console.log(funn(8)(9));
function dodaj(a) {
    return function (b) { return 4 * a * 5 * b; };
}
console.log(dodaj(9)(8));
var vfun = function (a) { return 7 * a; };
var fun1 = function (a) {
    return function (b) {
        return 2 * a + b;
    };
};
console.log(fun1(9)(2));
var Song = /** @class */ (function () {
    function Song() {
        this.title = "Highway to Hell";
        this.author = "ACDC";
    }
    Song.prototype.y = function () {
        return this;
    };
    return Song;
}());
var song = new Song();
console.log(song.y());
